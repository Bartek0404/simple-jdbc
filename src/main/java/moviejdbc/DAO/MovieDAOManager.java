package moviejdbc.DAO;

import moviejdbc.model.Movie;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MovieDAOManager implements MovieDAO {

    private final Connection connection;

    public MovieDAOManager(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void createTable() {
        try (final Statement createTableStatement = connection.createStatement()) {
            final String createTableQuery = "CREATE TABLE IF NOT EXISTS MOVIES (id INTEGER AUTO_INCREMENT, " +
                    "title VARCHAR(255), " +
                    "genre VARCHAR(255), " +
                    "yearOfRelease INTEGER, " +
                    "PRIMARY KEY (id))";
            createTableStatement.execute(createTableQuery);

        } catch (final SQLException exception) {
            throw new DatabaseActionException(exception);
        }
    }

    @Override
    public void deleteTable() {
        try (final Statement createTableStatement = connection.createStatement()) {
            final String deleteTableQuery = "DROP TABLE MOVIES";
            createTableStatement.execute(deleteTableQuery);
        } catch (final SQLException exception) {
            throw new DatabaseActionException(exception);
        }
    }

    @Override
    public void createMovie(Movie movie) {
        try (final PreparedStatement insertMovieStatement = connection.prepareStatement("INSERT INTO MOVIES (title,genre, yearOfRelease) VALUES (?,?,?)")) {
            insertMovieStatement.setString(1, movie.getTitle());
            insertMovieStatement.setString(2, movie.getGenre());
            insertMovieStatement.setInt(3, movie.getYearOfRelease());
            insertMovieStatement.executeUpdate();
        } catch (final SQLException exception) {
            throw new DatabaseActionException(exception);
        }
    }

    @Override
    public void deleteMovie(int id) {
        try (final PreparedStatement deleteMovieStatement = connection.prepareStatement("DELETE FROM MOVIES WHERE id = ?")){
            deleteMovieStatement.setInt(1, id);
            deleteMovieStatement.executeUpdate();
        } catch (final SQLException exception) {
            throw new DatabaseActionException(exception);
        }
    }

    @Override
    public void updateMoviesTitle(int id, String newTitle) {
        try (final PreparedStatement updateMovieStatement = connection.prepareStatement("UPDATE MOVIES SET title = ? WHERE id = ?")) {
            updateMovieStatement.setString(1, newTitle);
            updateMovieStatement.setInt(2, id);
            updateMovieStatement.executeUpdate();
        } catch (final SQLException exception) {
            throw new DatabaseActionException(exception);
        }
    }

    @Override
    public Optional<Movie> findMovieById(int id) {
        try (final PreparedStatement findMovieByIdStatement = connection.prepareStatement("SELECT * FROM MOVIES WHERE id = ?")) {
            findMovieByIdStatement.setInt(1, id);
            final boolean result = findMovieByIdStatement.execute();
            if (result) {
                ResultSet foundedMovie = findMovieByIdStatement.getResultSet();
                if (foundedMovie.next()) {
                    final String title = foundedMovie.getString(2);
                    final String genre = foundedMovie.getString(3);
                    final int yearOfRelease = foundedMovie.getInt(4);
                    return Optional.of(new Movie(id, title, genre, yearOfRelease));
                }
            }
        } catch (final SQLException exception) {
            throw new DatabaseActionException(exception);
        }
        return Optional.empty();
    }

    @Override
    public List<Movie> findAll() {
        try (Statement listAllStatement = connection.createStatement()) {
            final String findAllQuery = "SELECT * FROM MOVIES";
            listAllStatement.execute(findAllQuery);
            final List<Movie> movieList = new ArrayList<>();
            ResultSet resultSet = listAllStatement.getResultSet();
            while (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                String title = resultSet.getString(2);
                String genre = resultSet.getString(3);
                Integer yearOfRelease = resultSet.getInt(4);
                movieList.add(new Movie(id, title, genre, yearOfRelease));
            }
            return movieList;
        } catch (final SQLException exception) {
            throw new DatabaseActionException(exception);
        }
        //return null;
    }
}
