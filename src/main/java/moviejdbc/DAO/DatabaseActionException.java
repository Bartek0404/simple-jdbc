package moviejdbc.DAO;

public class DatabaseActionException extends RuntimeException {
    public DatabaseActionException(Throwable exception) {
        super(exception);
    }
}
