package moviejdbc;

import moviejdbc.DAO.MovieDAOManager;
import moviejdbc.model.Movie;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    private static final String MYSQL_DB_URL = "jdbc:mysql://localhost:3306/testdb";
    private static final String MYSQL_DB_USER = "root";
    private static final String MYSQL_DB_PASSWORD = "root";

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection(MYSQL_DB_URL, MYSQL_DB_USER, MYSQL_DB_PASSWORD)) {
            MovieDAOManager movieDAOManager = new MovieDAOManager(connection);
            movieDAOManager.deleteTable();
            movieDAOManager.createTable();
            movieDAOManager.createMovie(new Movie("Star Wars", "sci-fi",1976));
            movieDAOManager.createMovie(new Movie("Star Trek", "sci-fi",1982));
            movieDAOManager.createMovie(new Movie("Lord of the rings", "fantasy",2010));
            movieDAOManager.createMovie(new Movie("Saving Private Ryan", "war",2001));
            movieDAOManager.findAll().forEach(movie -> System.out.println(movie));
            movieDAOManager.updateMoviesTitle(1, "Star Wars IV");
            System.out.println(movieDAOManager.findMovieById(1));
            movieDAOManager.deleteMovie(3);
            movieDAOManager.findAll().forEach(movie -> System.out.println(movie));




        } catch (SQLException exception) {
            throw new RuntimeException();
        }
    }
}
